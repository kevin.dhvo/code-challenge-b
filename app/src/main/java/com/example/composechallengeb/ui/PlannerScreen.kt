package com.example.composechallengeb.ui

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.composechallengeb.R
import com.example.composechallengeb.ui.theme.ComposeChallengeBTheme
import com.example.composechallengeb.ui.theme.OffWhiteBackground

@Composable
fun PlannerScreen() {
    Box(modifier = Modifier
        .fillMaxSize()
        .background(OffWhiteBackground),
        contentAlignment = Alignment.Center
    ) {
        Column(
            modifier = Modifier,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                modifier = Modifier.padding(horizontal = 38.dp),
                text = "Hm, it doesn't look like you’ve any planned trip.",
                style = MaterialTheme.typography.h3.copy(lineHeight = 35.sp),
                textAlign = TextAlign.Center,
            )
            Image(
                painter = painterResource(id = R.drawable.flying_around_the_world_cuate),
                contentDescription = null,
                modifier = Modifier.padding(top = 12.dp)
            )
        }
    }
}


@Preview
@Composable
fun PlannerScreenPreview() {
    ComposeChallengeBTheme {
        Surface(modifier = Modifier.fillMaxSize()) {
            PlannerScreen()
        }
    }
}