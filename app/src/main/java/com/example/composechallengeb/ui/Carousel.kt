package com.example.composechallengeb.ui

import android.util.Log
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Color.Companion.Cyan
import androidx.compose.ui.graphics.Color.Companion.Green
import androidx.compose.ui.graphics.Color.Companion.Magenta
import androidx.compose.ui.graphics.Color.Companion.White
import androidx.compose.ui.graphics.Color.Companion.Yellow
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.zIndex
import com.example.composechallengeb.R
import com.example.composechallengeb.ui.theme.BlackText
import com.example.composechallengeb.ui.theme.ComposeChallengeBTheme
import com.example.composechallengeb.ui.theme.OffWhiteBackground
import com.example.composechallengeb.ui.theme.OrangeSurface
import kotlinx.coroutines.launch
import java.lang.Integer.max
import java.lang.Integer.min
import kotlin.math.roundToInt


@OptIn(ExperimentalMaterialApi::class)
@Composable
fun Carousel() {

    val swipeableState = rememberSwipeableState(0)
    val sizePx = with(LocalDensity.current) { 300.dp.toPx() }
    val anchors = mapOf(
        0f to 0,
        -sizePx to 1,
        -sizePx*2 to 2,
        -sizePx*3 to 3,
        -sizePx*4 to 4
    )

    var currentIndex by remember { mutableStateOf(0) }

    val travelCards: List<TravelCard> = listOf(
        TravelCard(
            image = R.drawable.picture,
            date = "Apr 24 - May 3",
            name = "Calgary - Vancouver",
            year = 2022,
            country = "Canada",
            duration = 10
        ),
        TravelCard(
            image = R.drawable.picture_1,
            date = "Apr 24 - May 3",
            name = "Florianópolis - Blumenau",
            year = 2022,
            country = "Brazil",
            duration = 10
        ),
        TravelCard(
            image = R.drawable.picture_2,
            date = "Apr 24 - May 3",
            name = "Rio - São Paulo",
            year = 2022,
            country = "Brazil",
            duration = 10
        ),
        TravelCard(
            image = R.drawable.picture_3,
            date = "Apr 24 - May 3",
            name = "Chicen Itza",
            year = 2022,
            country = "Mexico",
            duration = 10
        ),
        TravelCard(
            image = R.drawable.picture_4,
            date = "Apr 24 - May 3",
            name = "Miami",
            year = 2022,
            country = "USA",
            duration = 10
        ),
    )

    Box(modifier = Modifier
        .height(380.dp)
        .fillMaxWidth()
        .background(OffWhiteBackground)
        .swipeable(
            state = swipeableState,
            anchors = anchors,
            thresholds = { _, _ -> FractionalThreshold(0.3f) },
            orientation = Orientation.Horizontal,
            resistance = ResistanceConfig(0f, 0f, 0f)
        ),
        contentAlignment = Alignment.CenterStart
    ) {

        val scope = rememberCoroutineScope()
        if (swipeableState.isAnimationRunning) {
            DisposableEffect(Unit) {
                onDispose {
                    currentIndex += 1
                    if (swipeableState.currentValue == 4) {
                        scope.launch {
                            swipeableState.snapTo(0)
                        }
                    }
                }
            }
        }

        val firstCardSize by animateFloatAsState(
            targetValue = if (swipeableState.progress.fraction < 1 || swipeableState.targetValue >= 1) {
                when (swipeableState.targetValue) {
                    0 -> 1f
                    1 -> 0.8f
                    2 -> 0.8f
                    3 -> 0.9f
                    4 -> 1f
                    else -> 1f
                }
            } else {
                1f
            }
        )
        val firstCardOffset by animateDpAsState(
            targetValue = if (swipeableState.progress.fraction < 1 || swipeableState.targetValue >= 1) {
                when (swipeableState.targetValue) {
                    0 -> 0.dp
                    1 -> 70.dp
                    2 -> 140.dp
                    3 -> 70.dp
                    4 -> 0.dp
                    else -> 0.dp
                }
            } else {
                0.dp
            }
        )
        val firstCardZIndex by animateFloatAsState(
            targetValue = if (swipeableState.progress.fraction < 1
                || swipeableState.targetValue >= 1
                && !swipeableState.isAnimationRunning) {
                when (swipeableState.targetValue) {
                    0 -> 3f
                    1 -> 0f
                    2 -> 1f
                    3 -> 2f
                    4 -> 3f
                    else -> 3f
                }
            } else 3f
        )

        val secondCardSize by animateFloatAsState(
            targetValue = if (swipeableState.progress.fraction < 1 || swipeableState.targetValue >= 1) {
                when (swipeableState.targetValue) {
                    0 -> 0.9f
                    1 -> 1f
                    2 -> 0.8f
                    3 -> 0.8f
                    4 -> 0.9f
                    else -> 0.9f
                }
            } else {
                0.9f
            }
        )
        val secondCardOffset by animateDpAsState(
            targetValue = if (swipeableState.progress.fraction < 1 || swipeableState.targetValue >= 1) {
                when (swipeableState.targetValue) {
                    0 -> 70.dp
                    1 -> 0.dp
                    2 -> 70.dp
                    3 -> 140.dp
                    4 -> 70.dp
                    else -> 70.dp
                }
            } else {
                70.dp
            }
        )
        val secondCardZIndex by animateFloatAsState(
            targetValue = if (swipeableState.progress.fraction < 1
                || swipeableState.targetValue >= 1
                && !swipeableState.isAnimationRunning) {
                    when (swipeableState.targetValue) {
                        0 -> 2f
                        1 -> 3f
                        2 -> 0f
                        3 -> 1f
                        4 -> 2f
                        else -> 2f
                    }
            } else 2f
        )

        val thirdCardSize by animateFloatAsState(
            targetValue = if (swipeableState.progress.fraction < 1 || swipeableState.targetValue >= 1) {
                when (swipeableState.targetValue) {
                    0 -> 0.8f
                    1 -> 0.9f
                    2 -> 1f
                    3 -> 0.8f
                    4 -> 0.8f
                    else -> 0.8f
                }
            } else {
                0.8f
            }
        )
        val thirdCardOffset by animateDpAsState(
            targetValue = if (swipeableState.progress.fraction < 1 || swipeableState.targetValue >= 1) {
                when (swipeableState.targetValue) {
                    0 -> 140.dp
                    1 -> 70.dp
                    2 -> 0.dp
                    3 -> 70.dp
                    4 -> 140.dp
                    else -> 140.dp
                }
            } else {
                140.dp
            }
        )
        val thirdCardZIndex by animateFloatAsState(
            targetValue = if (swipeableState.progress.fraction < 1 ||
                swipeableState.targetValue >= 1
                && !swipeableState.isAnimationRunning) {
                    when (swipeableState.targetValue) {
                        0 -> 1f
                        1 -> 2f
                        2 -> 3f
                        3 -> 0f
                        4 -> 1f
                        else -> 1f
                    }
            } else 1f
        )

        val fourthCardSize by animateFloatAsState(
            targetValue = if (swipeableState.progress.fraction < 1 || swipeableState.targetValue >= 1) {
                when (swipeableState.targetValue) {
                    0 -> 0.8f
                    1 -> 0.8f
                    2 -> 0.9f
                    3 -> 1f
                    4 -> 0.8f
                    else -> 0.8f
                }
            } else {
                0.8f
            }
        )
        val fourthCardOffset by animateDpAsState(
            targetValue = if (swipeableState.progress.fraction < 1 || swipeableState.targetValue >= 1) {
                when (swipeableState.targetValue) {
                    0 -> 70.dp
                    1 -> 140.dp
                    2 -> 70.dp
                    3 -> 0.dp
                    4 -> 70.dp
                    else -> 0.dp
                }
            } else {
                0.dp
            }
        )
        val fourthCardZIndex by animateFloatAsState(
            targetValue = if (swipeableState.progress.fraction < 1 ||
                swipeableState.targetValue >= 1
                && !swipeableState.isAnimationRunning) {
                when (swipeableState.targetValue) {
                    0 -> 0f
                    1 -> 1f
                    2 -> 2f
                    3 -> 3f
                    4 -> 0f
                    else -> 0f
                }
            } else 0f
        )

        CustomCard(modifier = Modifier
            .fillMaxHeight(firstCardSize)
            .width((275 * firstCardSize).dp)
            .offset(firstCardOffset)
            .zIndex(firstCardZIndex),
            travelCard = travelCards[(currentIndex) % travelCards.size]
        )
        CustomCard(modifier = Modifier
            .fillMaxHeight(secondCardSize)
            .width((275 * secondCardSize).dp)
            .offset(secondCardOffset)
            .zIndex(secondCardZIndex),
            travelCard = travelCards[(currentIndex+1) % travelCards.size]
        )
        CustomCard(modifier = Modifier
            .fillMaxHeight(thirdCardSize)
            .width((275 * thirdCardSize).dp)
            .offset(thirdCardOffset)
            .zIndex(thirdCardZIndex),
            travelCard = travelCards[(currentIndex+2) % travelCards.size]
        )
        CustomCard(modifier = Modifier
            .fillMaxHeight(fourthCardSize)
            .width((275 * fourthCardSize).dp)
            .offset(fourthCardOffset)
            .zIndex(fourthCardZIndex),
            travelCard = travelCards[(currentIndex+3) % travelCards.size]
        )
    }
}

//https://stackoverflow.com/questions/63971569/androidautosizetexttype-in-jetpack-compose
@Composable
fun AutoResizeText(
    text: String,
    fontSizeRange: FontSizeRange,
    modifier: Modifier = Modifier,
    color: Color = Color.Unspecified,
    fontStyle: FontStyle? = null,
    fontWeight: FontWeight? = null,
    fontFamily: FontFamily? = null,
    letterSpacing: TextUnit = TextUnit.Unspecified,
    textDecoration: TextDecoration? = null,
    textAlign: TextAlign? = null,
    lineHeight: TextUnit = TextUnit.Unspecified,
    overflow: TextOverflow = TextOverflow.Clip,
    softWrap: Boolean = true,
    maxLines: Int = Int.MAX_VALUE,
    style: TextStyle = LocalTextStyle.current,
) {
    var fontSizeValue by remember { mutableStateOf(fontSizeRange.max.value) }
    var readyToDraw by remember { mutableStateOf(false) }

    Text(
        text = text,
        color = color,
        maxLines = maxLines,
        fontStyle = fontStyle,
        fontWeight = fontWeight,
        fontFamily = fontFamily,
        letterSpacing = letterSpacing,
        textDecoration = textDecoration,
        textAlign = textAlign,
        lineHeight = lineHeight,
        overflow = overflow,
        softWrap = softWrap,
        style = style,
        fontSize = fontSizeValue.sp,
        onTextLayout = {
            if (it.didOverflowHeight && !readyToDraw) {
                val nextFontSizeValue = fontSizeValue - fontSizeRange.step.value
                if (nextFontSizeValue <= fontSizeRange.min.value) {
                    // Reached minimum, set minimum font size and it's readToDraw
                    fontSizeValue = fontSizeRange.min.value
                    readyToDraw = true
                } else {
                    // Text doesn't fit yet and haven't reached minimum text range, keep decreasing
                    fontSizeValue = nextFontSizeValue
                }
            } else {
                // Text fits before reaching the minimum, it's readyToDraw
                readyToDraw = true
            }
        },
        modifier = modifier.drawWithContent { if (readyToDraw) drawContent() }
    )
}

data class FontSizeRange(
    val min: TextUnit,
    val max: TextUnit,
    val step: TextUnit = DEFAULT_TEXT_STEP,
) {
    init {
        require(min < max) { "min should be less than max, $this" }
        require(step.value > 0) { "step should be greater than 0, $this" }
    }

    companion object {
        private val DEFAULT_TEXT_STEP = 1.sp
    }
}

data class TravelCard(
    val image: Int,
    val date: String,
    val name: String,
    val year: Int,
    val country: String,
    val duration: Int
)


@Composable
fun CustomCard(modifier : Modifier = Modifier, travelCard: TravelCard) {
    Card(modifier = modifier,
        shape = RoundedCornerShape(31.dp)
    ) {
        Box(modifier = Modifier
            .fillMaxSize(),
        ) {
            Image(
                modifier = Modifier
                    .fillMaxHeight(0.55f)
                    .fillMaxWidth()
                    .align(Alignment.TopCenter),
                painter = painterResource(id = travelCard.image),
                contentDescription = null,
                contentScale = ContentScale.FillBounds
            )
            Box(
                modifier = Modifier
                    .fillMaxHeight(0.55f)
                    .fillMaxWidth()
                    .align(Alignment.BottomCenter)
                    .background(White, RoundedCornerShape(0.dp, 31.dp, 31.dp, 31.dp))
            ) {
                Surface(modifier = Modifier.padding(top = 20.dp),
                    color = OrangeSurface,
                    contentColor = White,
                    shape = RoundedCornerShape(0.dp,25.dp,25.dp,0.dp)
                ) {
                    AutoResizeText(
                        modifier = Modifier
                            .padding(12.dp, 12.dp, 21.dp, 12.dp),
                        text = travelCard.date,
                        style = MaterialTheme.typography.subtitle2.copy(fontWeight = FontWeight.SemiBold),
                        fontSizeRange = FontSizeRange(
                            min = 8.sp,
                            max = 12.sp,
                        )
                    )
                }
                Column(modifier = Modifier
                    .padding(horizontal = 12.dp)
                    .padding(top = 75.dp)
                ) {
                    AutoResizeText(
                        modifier = Modifier,
                        text = travelCard.name,
                        style = MaterialTheme.typography.h2,
                        color = BlackText,
                        fontSizeRange = FontSizeRange(
                            min = 8.sp,
                            max = 18.sp,
                        )
                    )
                    AutoResizeText(
                        modifier = Modifier,
                        text = travelCard.year.toString(),
                        style = MaterialTheme.typography.h2,
                        color = BlackText,
                        fontSizeRange = FontSizeRange(
                            min = 8.sp,
                            max = 18.sp,
                        )
                    )
                    Row(
                        modifier = Modifier.padding(bottom = 26.dp),
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Icon(
                            modifier = Modifier.size(28.dp),
                            painter = painterResource(id = R.drawable.icon_marker),
                            contentDescription = null
                        )
                        AutoResizeText(
                            modifier = Modifier.offset((-3).dp),
                            text = travelCard.country,
                            style = MaterialTheme.typography.subtitle2,
                            color = BlackText,
                            fontSizeRange = FontSizeRange(
                                min = 8.sp,
                                max = 12.sp,
                            )
                        )
                        Spacer(modifier = Modifier.weight(1f))
                        AutoResizeText(
                            modifier = Modifier,
                            text = "${travelCard.duration} days",
                            style = MaterialTheme.typography.subtitle2,
                            color = BlackText,
                            fontSizeRange = FontSizeRange(
                                min = 8.sp,
                                max = 12.sp,
                            )
                        )
                    }
                }
            }
        }
    }
}

@Preview
@Composable
fun CustomCardPreview() {
    ComposeChallengeBTheme() {
        CustomCard(modifier = Modifier.size(275.dp, 380.dp), travelCard =
            TravelCard(
                image = R.drawable.picture,
                date = "Apr 24 - May 3",
                name = "Calgary - Vancouver",
                year = 2022,
                country = "Canada",
                duration = 10
            )
        )
    }
}


@Preview
@Composable
fun CarouselPreview() {
    Carousel()
}