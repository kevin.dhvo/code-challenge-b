package com.example.composechallengeb.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

val OffWhiteBackground = Color(0xFFF4F4F4)
val YellowOrange = Color(0xFFFA9328)
val YellowTintedWhite = Color(0xFFFEE9D3)
val BlackText = Color(0xFF2C2C2C)
val GreyText = Color(0xFF565656)

val OrangeSurface = Color(0xFFFA9125)