package com.example.composechallengeb.ui

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.ScrollableTabRow
import androidx.compose.material.Surface
import androidx.compose.material.TabRowDefaults.tabIndicatorOffset
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.zIndex
import com.example.composechallengeb.R
import com.example.composechallengeb.ui.theme.ComposeChallengeBTheme
import com.example.composechallengeb.ui.theme.OffWhiteBackground
import com.example.composechallengeb.ui.theme.YellowTintedWhite
import java.sql.Time

@Composable
fun RouteDetailScreen(
    navigateBack: () -> Unit = { }
) {
    Box(modifier = Modifier
        .fillMaxSize()
        .background(Color.Black)) {

        // Background
        Image(
            painter = painterResource(id = R.drawable.timeline_header),
            contentDescription = null,
            contentScale = ContentScale.Crop,
            modifier = Modifier
                .zIndex(-5f)
                .fillMaxWidth()
        )

        // Header
        Row(modifier = Modifier
            .fillMaxWidth()
            .padding(top = 10.dp)
            .padding(horizontal = 24.dp),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Box(modifier = Modifier
                .size(48.dp)
                .clickable { navigateBack() },
                contentAlignment = Alignment.Center
            ) {
                Icon(
                    modifier = Modifier.padding(horizontal = 12.dp),
                    imageVector = Icons.Default.ArrowBack,
                    contentDescription = null
                )
            }
            Box(modifier = Modifier.size(48.dp), contentAlignment = Alignment.Center) {
                Icon(
                    modifier = Modifier,
                    painter = painterResource(id = R.drawable.icon_heart),
                    contentDescription = null
                )
            }
        }

        // Details
        Column(modifier = Modifier
            .fillMaxSize()
            .padding(top = 250.dp)
            .background(OffWhiteBackground, RoundedCornerShape(topEnd = 30.dp))
        ) {

            var selectedTab by remember { mutableStateOf(DetailTabs.TIMELINE) }

            ScrollableTabRow(
                modifier = Modifier
                    .padding(top = 10.dp)
                    .height(48.dp)
                    .clip(RoundedCornerShape(topEnd = 30.dp)),
                edgePadding = 0.dp,
                backgroundColor = OffWhiteBackground,
                selectedTabIndex = selectedTab.ordinal,
                indicator = { tabPositions ->
                    Surface(
                        modifier = Modifier
                            .tabIndicatorOffset(tabPositions[selectedTab.ordinal])
                            .fillMaxSize()
                            .zIndex(-1f),
                        color = YellowTintedWhite,
                        shape = RoundedCornerShape(25.dp)
                    ) {}
                },
                divider = {}
            ) {
                DetailTabs.values().forEach { detail ->
                    CustomTab(
                        title = detail.title,
                        selected = detail.ordinal == selectedTab.ordinal,
                        onClick = {selectedTab = detail}
                    )
                }
            }

            Spacer(modifier = Modifier.height(12.dp))
            when (selectedTab) {
                DetailTabs.INFO -> {
                    Box(modifier = Modifier.fillMaxSize()) {
                        Text("Info")
                    }
                }
                DetailTabs.TIMELINE -> {
                    Timeline()
                }
                DetailTabs.BUDGET -> {
                    Box(modifier = Modifier.fillMaxSize()) {
                        Text("Budget")
                    }
                }
            }
        }
    }
}

enum class DetailTabs(val title: String) {
    INFO("Information"),
    TIMELINE("Timeline"),
    BUDGET("Budget")
}

@Preview (showBackground = true)
@Composable
fun RouteDetailScreenPreview() {
    ComposeChallengeBTheme() {
        RouteDetailScreen()
    }
}