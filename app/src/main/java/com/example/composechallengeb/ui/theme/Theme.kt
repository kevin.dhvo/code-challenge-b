package com.example.composechallengeb.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

private val DarkColorPalette = darkColors(
    primary = YellowOrange,
    primaryVariant = YellowTintedWhite,
    secondary = Color.White,
    background = Color.White,
    surface = OffWhiteBackground,
    onSurface = BlackText

)

private val LightColorPalette = lightColors(
    primary = YellowOrange,
    primaryVariant = YellowTintedWhite,
    secondary = Color.White,
    background = Color.White,
    surface = OffWhiteBackground,
    onSurface = BlackText

    /* Other default colors to override
    background = Color.White,
    surface = Color.White,
    onPrimary = Color.White,
    onSecondary = Color.Black,
    onBackground = Color.Black,
    onSurface = Color.Black,
    */
)

@Composable
fun ComposeChallengeBTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    val colors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }

    MaterialTheme(
        colors = colors,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}