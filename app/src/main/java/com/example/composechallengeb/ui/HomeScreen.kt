package com.example.composechallengeb.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.TabRowDefaults.tabIndicatorOffset
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.zIndex
import com.example.composechallengeb.R
import com.example.composechallengeb.ui.theme.*

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun HomeScreen(
    navigateRouteDetail: (TravelCard) -> Unit = { }
) {
    Column(modifier = Modifier
        .verticalScroll(rememberScrollState())
    ) {

        Text(
            modifier = Modifier,
            text = "Where do\nyou want to go?",
            style = MaterialTheme.typography.h1.copy(lineHeight = 35.sp)
        )

        // Search bar
        var searchText by remember { mutableStateOf("") }
        val keyboardController = LocalSoftwareKeyboardController.current

        TextField(
            modifier = Modifier
                .padding(top = 10.dp)
                .fillMaxWidth()
                .height(48.dp),
            shape = RoundedCornerShape(15.dp),
            colors = TextFieldDefaults.textFieldColors(
                textColor = BlackText,
                placeholderColor = GreyText,
                backgroundColor = Color.White,
                focusedIndicatorColor = Color.Transparent,
                unfocusedIndicatorColor = Color.Transparent,
                disabledIndicatorColor = Color.Transparent
            ),
            leadingIcon = {
                Icon(
                    painter = painterResource(id = R.drawable.magnifying_glass),
                    contentDescription = null,
                    modifier = Modifier.size(48.dp),
                    tint = Color.Black
                )
            },
            trailingIcon = {
                Box() {
                    Divider(
                        modifier = Modifier
                            .fillMaxHeight()
                            .padding(vertical = 6.dp)
                            .size(1.dp)
                            .align(Alignment.CenterStart)
                    )
                    Icon(
                        painter = painterResource(id = R.drawable.filters),
                        contentDescription = null,
                        modifier = Modifier.size(48.dp),
                        tint = Color.Black
                    )
                }
            },
            placeholder = {
                Text(
                    text = "Search a route",
                    style = MaterialTheme.typography.subtitle2
                )
            },
            singleLine = true,
            textStyle = MaterialTheme.typography.subtitle2,
            keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
            keyboardActions = KeyboardActions(
                onDone = { keyboardController?.hide() }
            ),
            value = searchText, onValueChange = {searchText = it}
        )

        // Destinations Tabs
        var selectedTab by remember { mutableStateOf(DestinationTabs.ALL) }
        ScrollableTabRow(
            modifier = Modifier
                .padding(top = 10.dp)
                .height(48.dp),
            edgePadding = 0.dp,
            backgroundColor = OffWhiteBackground,
            selectedTabIndex = selectedTab.ordinal,
            indicator = { tabPositions ->
                Surface(
                    modifier = Modifier
                        .tabIndicatorOffset(tabPositions[selectedTab.ordinal])
                        .fillMaxSize()
                        .zIndex(-1f),
                    color = YellowTintedWhite,
                    shape = RoundedCornerShape(25.dp)
                ) {}
            },
            divider = {}
        ) {
            DestinationTabs.values().forEach { destination ->
                CustomTab(
                    title = destination.title,
                    selected = destination.ordinal == selectedTab.ordinal,
                    onClick = {selectedTab = destination}
                )
            }
        }

        // Carousel
        Spacer(modifier = Modifier.height(12.dp))
        when (selectedTab) {
            DestinationTabs.ALL -> {
                CarouselB(
                    onItemClick = { item -> navigateRouteDetail(item) }
                )
                Spacer(modifier = Modifier.height(12.dp))
            }
            DestinationTabs.AMERICA -> {
                Box(modifier = Modifier
                    .fillMaxWidth()
                    .height(380.dp)
                    .background(YellowTintedWhite, RoundedCornerShape(50.dp)),
                    contentAlignment = Alignment.Center
                ) {
                    Text("AMERICA")
                }
            }
            DestinationTabs.ASIA -> {
                Box(modifier = Modifier
                    .fillMaxWidth()
                    .height(380.dp)
                    .background(YellowTintedWhite, RoundedCornerShape(50.dp)),
                    contentAlignment = Alignment.Center
                ) {
                    Text("ASIA")
                }
            }
            DestinationTabs.EUROPE -> {
                Carousel()
                Spacer(modifier = Modifier.height(12.dp))
            }
        }
    }
}

@Composable
fun CustomTab(
    title: String,
    selected: Boolean,
    onClick: () -> Unit,
) {
    Text(
        modifier = Modifier
            .padding(horizontal = 40.dp, vertical = 14.dp)
            .clickable(
                interactionSource = remember { MutableInteractionSource() },
                indication = null,
                onClick = { onClick() }
            ),
        text = title,
        style = MaterialTheme.typography.h4.copy(
            fontWeight = if (selected) FontWeight.Bold else FontWeight.SemiBold,
            color = if (selected) YellowOrange else BlackText
        )
    )
}

enum class DestinationTabs(val title: String) {
    ALL("All"),
    AMERICA("America"),
    ASIA("Asia"),
    EUROPE("Europe")
}

@Preview
@Composable
fun HomeScreenPreview() {
    ComposeChallengeBTheme {
        Surface(modifier = Modifier.fillMaxSize()) {
            HomeScreen()
        }
    }
}