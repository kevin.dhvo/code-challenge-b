package com.example.composechallengeb.ui

import android.util.Log
import androidx.annotation.DrawableRes
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.PathEffect
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.zIndex
import com.example.composechallengeb.R
import com.example.composechallengeb.ui.theme.ComposeChallengeBTheme
import com.example.composechallengeb.ui.theme.OffWhiteBackground
import com.example.composechallengeb.ui.theme.YellowOrange

val timelineItems: List<TimelineItem> = listOf(
    TimelineItem(
        day = "Day 1",
        date = "Apr 24",
        image = R.drawable.timeline_1,
        desc = "Arrival and check in Hotel",
        location = "Calgary, Canada"
    ),
    TimelineItem(
        day = "Day 2",
        date = "Apr 25"
    ),
    TimelineItem(
        day = "Day 3",
        date = "Apr 26",
        image = R.drawable.timeline_2,
        desc = "Check in second Hotel",
        location = "Calgary, Canada"
    ),
    TimelineItem(
        day = "Day 4",
        date = "Apr 27",
        image = R.drawable.timeline_3,
        desc = "Go to the beach",
        location = "Calgary, Canada"
    ),
    TimelineItem(
        day = "Day 5",
        date = "Apr 28"
    ),
    TimelineItem(
        day = "Day 6",
        date = "Apr 29"
    ),
    TimelineItem(
        day = "Day 7",
        date = "Apr 30",
        image = R.drawable.timeline_1,
        desc = "Go back home",
        location = "Calgary, Canada"
    )
)

val pathEffect = PathEffect.dashPathEffect(floatArrayOf(10f, 10f), 0f)

@Composable
fun Timeline() {
    Box(modifier = Modifier) {

        LazyColumn(
            modifier = Modifier,
        ) {
            itemsIndexed(timelineItems) { index, item ->
                TimelineRow(
                    timelineItem = item,
                    isFirst = index == 0,
                    isLast = timelineItems.size == index+1
                )
            }
        }
    }
}

data class TimelineItem(
    val day: String,
    val date: String,
    @DrawableRes val image: Int? = null,
    val desc: String? = null,
    val location: String? = null
)

@Composable
fun TimelineRow(
    timelineItem: TimelineItem,
    isFirst: Boolean = false,
    isLast: Boolean = false
) {
    // Date
    Box(modifier = Modifier
        .fillMaxWidth()
        .background(OffWhiteBackground)
    ) {
        Column(modifier = Modifier.fillMaxSize()) {

            val modifier = if (timelineItem.image != null) {
                Modifier.height(130.dp)
            } else {
                Modifier
            }
            Row(
                modifier = modifier,
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth(0.4f),
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Column(
                        modifier = Modifier
                            .padding(start = 20.dp)
                            .fillMaxHeight()
                            .fillMaxWidth(0.4f),
                        verticalArrangement = Arrangement.Center
                    ) {
                        Text(
                            modifier = Modifier.fillMaxWidth(),
                            text = timelineItem.day,
                            style = MaterialTheme.typography.body1.copy(
                                color = YellowOrange,
                                fontWeight = FontWeight.SemiBold,
                                fontSize = 14.sp
                            ),
                            textAlign = TextAlign.Center
                        )
                        Text(
                            modifier = Modifier.fillMaxWidth(),
                            text = timelineItem.date,
                            style = MaterialTheme.typography.body1.copy(
                                color = YellowOrange,
                                fontWeight = FontWeight.SemiBold,
                                fontSize = 14.sp
                            ),
                            textAlign = TextAlign.Center
                        )
                    }

                    if (timelineItem.image != null && timelineItem.desc != null && timelineItem.location != null) {
                        Box(modifier = Modifier
                            .fillMaxWidth()
                            .fillMaxHeight()
                        ) {
                            if (!isFirst) {
                                Canvas(modifier = Modifier
                                    .fillMaxHeight()
                                    .width(2.dp)
                                    .align(Alignment.TopCenter)
                                ) {
                                    drawLine(
                                        color = Color.Black,
                                        start = Offset(0f, 0f),
                                        end = Offset(0f, if (!isFirst && !isLast) size.height/2-6.dp.toPx() else size.height/2-12.dp.toPx()),
                                        pathEffect = pathEffect
                                    )
                                }
                            }
                            Icon(
                                painter = painterResource(
                                    id = if (isFirst) R.drawable.icon_marker else if (isLast) R.drawable.icon_flag_pennant else R.drawable.icon_circle
                                ),
                                contentDescription = null,
                                modifier = Modifier
                                    .size(48.dp)
                                    .padding(if (isFirst || isLast) 12.dp else 18.dp)
                                    .align(Alignment.Center),
                                tint = YellowOrange
                            )

                            if (!isLast) {
                                Canvas(modifier = Modifier
                                    .fillMaxHeight()
                                    .width(2.dp)
                                    .align(Alignment.Center)
                                ) {
                                    drawLine(
                                        color = Color.Black,
                                        start = Offset(0f, if (!isFirst && !isLast) size.height/2+6.dp.toPx() else size.height/2+12.dp.toPx()),
                                        end = Offset(0f, size.height),
                                        pathEffect = pathEffect
                                    )
                                }
                            }
                        }
                    } else {
                        Box(modifier = Modifier
                            .fillMaxWidth()
                            .height(48.dp),
                            contentAlignment = Alignment.Center
                        ) {
                            Canvas(modifier = Modifier
                                .fillMaxHeight()
                                .width(2.dp)
                            ) {
                                drawLine(
                                    color = Color.Black,
                                    start = Offset(0f, 0f),
                                    end = Offset(0f, size.height),
                                    pathEffect = pathEffect
                                )
                            }
                        }
                    }
                }

                timelineItem.image?.let { image ->
                    Image(
                        modifier = Modifier
                            .height(130.dp)
                            .clip(RoundedCornerShape(topStart = 20.dp, bottomStart = 20.dp)),
                        contentDescription = null,
                        painter = painterResource(id = image),
                        contentScale = ContentScale.None
                    )
                }
            }

            Row(modifier = Modifier
                .fillMaxSize()
                .drawWithContent {
                    val space = size.width*0.4f-20.dp.toPx()
                    val xOffset1 = space*0.4f
                    val xOffset2 = xOffset1+space*0.3f

                    if (!isLast) {
                        drawLine(
                            color = Color.Black,
                            start = Offset(xOffset2+20.dp.toPx()-3.25f, 0f),
                            end = Offset(xOffset2+20.dp.toPx()-3.25f, size.height),
                            pathEffect = pathEffect
                        )
                    }
                    drawContent()
                },
            ) {
                Column(Modifier.fillMaxWidth(0.4f)){}
                Column(modifier = Modifier
                    .fillMaxWidth()
                ) {
                    timelineItem.desc?.let { desc ->
                        timelineItem.location?.let { location ->
                            Text(
                                modifier = Modifier
                                    .padding(start = 20.dp, top = 6.dp),
                                text = desc,
                                style = MaterialTheme.typography.body1.copy(
                                    fontSize = 14.sp
                                )
                            )
                            Text(
                                modifier = Modifier
                                    .padding(start = 20.dp),
                                text = location,
                                style = MaterialTheme.typography.body1.copy(
                                    fontSize = 12.sp
                                )
                            )
                        }
                    }
                }
            }
        }
    }
}


@Composable
@Preview (showBackground = true)
fun TimelinePreview() {
    ComposeChallengeBTheme() {
        Timeline()
    }
}

@Composable
@Preview (showBackground = true)
fun TimelineItemPreview() {
    ComposeChallengeBTheme {
        TimelineRow(
            TimelineItem(
                day = "Day 1",
                date = "Apr 24",
            )
        )
    }
}