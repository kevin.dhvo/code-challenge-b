package com.example.composechallengeb.ui

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.PointerEventPass
import androidx.compose.ui.input.pointer.PointerInputChange
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.zIndex
import com.example.composechallengeb.R
import com.example.composechallengeb.ui.theme.BlackText
import com.example.composechallengeb.ui.theme.ComposeChallengeBTheme
import com.example.composechallengeb.ui.theme.OrangeSurface

@OptIn(ExperimentalMaterialApi::class, ExperimentalFoundationApi::class)
@Composable
fun CarouselB(
    onItemClick: (TravelCard) -> Unit = { }
) {
    val items = remember { mutableStateListOf(
        TravelCard(
            image = R.drawable.picture,
            date = "Apr 24 - May 3",
            name = "Calgary - Vancouver",
            year = 2022,
            country = "Canada",
            duration = 10
        ),
        TravelCard(
            image = R.drawable.picture_1,
            date = "Apr 24 - May 3",
            name = "Florianópolis - Blumenau",
            year = 2022,
            country = "Brazil",
            duration = 10
        ),
        TravelCard(
            image = R.drawable.picture_2,
            date = "Apr 24 - May 3",
            name = "Rio - São Paulo",
            year = 2022,
            country = "Brazil",
            duration = 10
        ),
        TravelCard(
            image = R.drawable.picture_3,
            date = "Apr 24 - May 3",
            name = "Chicen Itza",
            year = 2022,
            country = "Mexico",
            duration = 10
        ),
        TravelCard(
            image = R.drawable.picture_4,
            date = "Apr 24 - May 3",
            name = "Miami",
            year = 2022,
            country = "USA",
            duration = 10
        )
    ) }
    var count by remember { mutableStateOf(0) }

    LazyRow(
        userScrollEnabled = false,
        horizontalArrangement = Arrangement.spacedBy((-210).dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        itemsIndexed(
            items,
            {_,item -> item.name+count }
        ) { i, item ->

            val dismissState = rememberDismissState(DismissValue.Default)
            if (dismissState.isDismissed(DismissDirection.EndToStart)) {
                items.remove(item)
                items.add(item)
                count++
            }

            SwipeToDismiss(
                state = dismissState,
                modifier = Modifier
                    .zIndex(
                        when (i) {
                            0 -> 1f
                            1 -> 0.9f
                            2 -> 0.8f
                            else -> -1f
                        }
                    )
                    .animateItemPlacement(),
                directions = setOf(
                    DismissDirection.EndToStart
                ),
                dismissThresholds = { direction ->
                    FractionalThreshold(if (direction == DismissDirection.EndToStart) 0.1f else 0.05f)
                },
                background = { },
                dismissContent = {
                    Box(modifier = Modifier
                        .size(275.dp,380.dp),
                        contentAlignment = Alignment.CenterStart
                    ) {
                        val modifier = Modifier
                            .fillMaxSize(
                                when (i) {
                                    0 -> 1f
                                    1 -> 0.9f
                                    2 -> 0.8f
                                    else -> 0f
                                }
                            )
                        if (items.isNotEmpty() && i == 0) {
                            TravelCard(
                                modifier = modifier.clickable { onItemClick(item) },
                                travelCard = item,
                                position = i
                            )
                        } else {
                            TravelCard(
                                modifier = modifier.gesturesDisabled(true),
                                travelCard = item,
                                position = i
                            )
                        }
                    }
                }
            )
        }
    }
}

@Composable
fun TravelCard(modifier : Modifier = Modifier, travelCard: TravelCard, position: Int = 0) {
    Card(modifier = modifier,
        shape = RoundedCornerShape(31.dp)
    ) {
        Box(modifier = Modifier
            .fillMaxSize(),
        ) {
            Image(
                modifier = Modifier
                    .fillMaxHeight(0.55f)
                    .fillMaxWidth()
                    .align(Alignment.TopCenter),
                painter = painterResource(id = travelCard.image),
                contentDescription = null,
                contentScale = ContentScale.FillBounds
            )
            Box(
                modifier = Modifier
                    .fillMaxHeight(0.55f)
                    .fillMaxWidth()
                    .align(Alignment.BottomCenter)
                    .background(Color.White, RoundedCornerShape(0.dp, 31.dp, 31.dp, 31.dp))
            ) {
                Surface(modifier = Modifier.padding(top = 20.dp),
                    color = OrangeSurface,
                    contentColor = Color.White,
                    shape = RoundedCornerShape(0.dp,25.dp,25.dp,0.dp)
                ) {
                    val paddingValues = when (position) {
                        0 -> PaddingValues(12.dp, 12.dp, 21.dp, 12.dp)
                        1 -> PaddingValues(10.dp, 10.dp, 19.dp, 10.dp)
                        2 -> PaddingValues(8.dp, 8.dp, 17.dp, 8.dp)
                        else -> PaddingValues(8.dp, 8.dp, 17.dp, 8.dp)
                    }
                    Text(
                        modifier = Modifier
                            .padding(paddingValues),
                        text = travelCard.date,
                        style = MaterialTheme.typography.subtitle2.copy(fontWeight = FontWeight.SemiBold),
                        fontSize = when (position) {
                            0 -> 12.sp
                            1 -> 10.sp
                            2 -> 8.sp
                            else -> 8.sp
                        }
                    )
                }
                Column(modifier = Modifier
                    .padding(horizontal = 12.dp)
                    .padding(top = 75.dp)
                ) {
                    Text(
                        modifier = Modifier,
                        text = travelCard.name,
                        style = MaterialTheme.typography.h2,
                        color = BlackText,
                        fontSize = when (position) {
                            0 -> 18.sp
                            1 -> 16.sp
                            2 -> 14.sp
                            else -> 12.sp
                        }
                    )
                    Text(
                        modifier = Modifier,
                        text = travelCard.year.toString(),
                        style = MaterialTheme.typography.h2,
                        color = BlackText,
                        fontSize = when (position) {
                            0 -> 18.sp
                            1 -> 16.sp
                            2 -> 14.sp
                            else -> 12.sp
                        }
                    )
                    Row(
                        modifier = Modifier.padding(bottom = 26.dp),
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Icon(
                            modifier = Modifier.size(28.dp),
                            painter = painterResource(id = R.drawable.icon_marker),
                            contentDescription = null
                        )
                        Text(
                            modifier = Modifier.offset((-3).dp),
                            text = travelCard.country,
                            style = MaterialTheme.typography.subtitle2,
                            color = BlackText,
                            fontSize = when (position) {
                                0 -> 12.sp
                                1 -> 10.sp
                                2 -> 8.sp
                                else -> 8.sp
                            }
                        )
                        Spacer(modifier = Modifier.weight(1f))
                        Text(
                            modifier = Modifier,
                            text = "${travelCard.duration} days",
                            style = MaterialTheme.typography.subtitle2,
                            color = BlackText,
                            fontSize = when (position) {
                                0 -> 12.sp
                                1 -> 10.sp
                                2 -> 8.sp
                                else -> 8.sp
                            }
                        )
                    }
                }
            }
        }
    }
}

fun Modifier.gesturesDisabled(disabled: Boolean = true) =
    if (disabled) {
        pointerInput(Unit) {
            awaitPointerEventScope {
                // we should wait for all new pointer events
                while (true) {
                    awaitPointerEvent(pass = PointerEventPass.Initial)
                        .changes
                        .forEach(PointerInputChange::consume)
                }
            }
        }
    } else {
        Modifier
    }


@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    ComposeChallengeBTheme() {
        Surface(Modifier.fillMaxSize()) {
            CarouselB()
        }
    }
}