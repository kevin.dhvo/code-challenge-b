package com.example.composechallengeb

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.*
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.TabRowDefaults.tabIndicatorOffset
import com.example.composechallengeb.ui.TabRowDefaults.tabIndicatorOffset
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.Center
import androidx.compose.ui.Alignment.Companion.CenterStart
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.ExperimentalTextApi
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.zIndex
import androidx.navigation.NavController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.composechallengeb.ui.CustomScrollableTabRow
import com.example.composechallengeb.ui.HomeScreen
import com.example.composechallengeb.ui.PlannerScreen
import com.example.composechallengeb.ui.RouteDetailScreen
import com.example.composechallengeb.ui.theme.*

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {

            ComposeChallengeBTheme {

                val navController = rememberNavController()

                NavHost(navController = navController, startDestination = "root") {
                    composable("root") { Content(navController) }
                    composable("routeDetails") { RouteDetailScreen(
                        navigateBack = { navController.navigateUp() }
                    ) }
                }
            }
        }
    }
}

@Composable
fun Content(navController: NavController) {
    Surface(modifier = Modifier
        .fillMaxSize(),
        color = OffWhiteBackground
    ) {
        val scaffoldState = rememberScaffoldState()
        var selectedNavTab by remember { mutableStateOf(NavigationTabs.HOME) }
        Scaffold(modifier = Modifier,
            scaffoldState = scaffoldState,
            backgroundColor = OffWhiteBackground,
            topBar = {
                Row(
                    modifier = Modifier
                        .padding(vertical = 16.dp, horizontal = 24.dp),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Icon(
                        painter = painterResource(id = R.drawable.cat),
                        contentDescription = null,
                        tint = Color.Unspecified,
                        modifier = Modifier.size(48.dp)
                    )
                    Text(
                        modifier = Modifier.padding(start = 8.dp),
                        text = "Hi, Chloe!",
                        style = MaterialTheme.typography.h4.copy(color = YellowOrange)
                    )
                    Spacer(modifier = Modifier.weight(1f))
                    Icon(
                        painter = painterResource(id = R.drawable.bell),
                        contentDescription = null,
                        tint = Color.Black,
                        modifier = Modifier.size(48.dp)
                    )
                }
            },
            bottomBar = {
                // Bottom Nav Tabs
                CustomScrollableTabRow(
                    modifier = Modifier
                        .padding(vertical = 11.dp, horizontal = 10.dp)
                        .height(48.dp),
                    edgePadding = 0.dp,
                    backgroundColor = OffWhiteBackground,
                    selectedTabIndex = selectedNavTab.ordinal,
                    indicator = { tabPositions ->
                        Surface(
                            modifier = Modifier
                                .tabIndicatorOffset(tabPositions[selectedNavTab.ordinal])
                                .size(115.dp, 48.dp)
                                .zIndex(-1f),
                            color = YellowTintedWhite,
                            shape = RoundedCornerShape(25.dp)
                        ) {}
                    },
                    divider = {}
                ) {
                    NavigationTabs.values().forEach { destination ->
                        BottomNavTab(
                            title = destination.title,
                            icon = destination.icon,
                            selected = destination.ordinal == selectedNavTab.ordinal,
                            onClick = {selectedNavTab = destination}
                        )
                    }
                }
            }
        ) { bottomPadding ->
            Box(modifier = Modifier
                .fillMaxSize()
                .padding(bottomPadding)
                .padding(horizontal = 24.dp)
            ) {
                when (selectedNavTab) {
                    NavigationTabs.HOME -> HomeScreen(navigateRouteDetail = { navController.navigate("routeDetails") })
                    NavigationTabs.PLANNER -> PlannerScreen()
                    NavigationTabs.LIKES -> RouteDetailScreen()
                    else -> {}
                }
            }
        }
    }
}

@Composable
fun BottomNavTab(
    title: String,
    icon: Int,
    selected: Boolean,
    onClick: () -> Unit
) {
    Row(modifier = Modifier
        .clickable(
            interactionSource = remember { MutableInteractionSource() },
            indication = null,
            onClick = { onClick() }
        ),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Icon(
            painter = painterResource(id = icon),
            contentDescription = null,
            tint = if (selected) YellowOrange else BlackText
        )
        AnimatedVisibility(
            visible = selected
        ) {
            Text(
                modifier = Modifier
                    .align(CenterVertically)
                    .padding(end = 22.dp),
                text = title,
                style = MaterialTheme.typography.h5.copy(
                    color = if (selected) YellowOrange else BlackText
                )
            )
        }
    }
}

enum class NavigationTabs(val title: String, val icon: Int) {
    HOME("Home", R.drawable.icon_home),
    PLANNER("Planner", R.drawable.icon_calendar),
    LIKES("Likes", R.drawable.icon_heart),
    PROFILE("Profile", R.drawable.icon_user)
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    ComposeChallengeBTheme {
        Content(NavController(LocalContext.current))
    }
}